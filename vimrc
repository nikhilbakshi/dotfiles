set nocompatible              " be iMproved, required
filetype off                  " required

if has('python3')
endif

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo

Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'Raimondi/delimitMate'
Plugin 'Yggdroot/indentLine'
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'
Plugin 'scrooloose/syntastic'
Plugin 'tomtom/tlib_vim'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'kchmck/vim-coffee-script'
Plugin 'altercation/vim-colors-solarized'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-fugitive'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'pangloss/vim-javascript'
Plugin 'mxw/vim-jsx'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'tpope/vim-surround'
Plugin 'Valloric/YouCompleteMe'
Plugin 'ross/requests-futures'
Plugin 'Valloric/ycmd'
Plugin 'ternjs/tern_for_vim'
Plugin 'pearofducks/ansible-vim'
Plugin 'sbdchd/neoformat'
Plugin 'matthew-brett/vim-rst-sections'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

syntax on

set number
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab


" Enable mouse use in all modes
set mouse=a
set ttymouse=xterm2

" Settings for NERDTreeTabs
" open on startup
let g:nerdtree_tabs_open_on_console_startup=1
let NERDTreeDirArrows=1
nmap ,m :NERDTreeToggle<CR>
nmap ,n :NERDTreeFind<CR>
" To allow user to switch buffers without saving contents of existing buffer
set hidden

" Highlight all words in file, which match the word being searched
set hlsearch
nmap ,h :nohlsearch<CR>

" Close Tags
" act on filenames like *.xml, *.html, *.xhtml, ...
let g:closetag_filenames = '*.html,*.xhtml,*.phtml,*.jsx'
" filenames like *.xml, *.xhtml, ...
let g:closetag_xhtml_filenames = '*.xhtml,*.jsx'

" Settings for airline
set laststatus=2
let g:airline_theme='wombat'
"let &t_Co=256
let g:airline#extensions#tabline#enabled = 1
" Just show the filename (no path) in the tab
let g:airline#extensions#tabline#fnamemod = ':t'
set term=screen-256color

" Settings for CtrlP
set runtimepath^=~/.vim/bundle/ctrlp.vim
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
"let g:ctrlp_prompt_mappings = {
"    \ 'AcceptSelection("e")': ['<c-t>'],
"    \ 'AcceptSelection("t")': ['<cr>', '<2-LeftMouse>'],
"    \ }
let g:ctrlp_custom_ignore = 'node_modules\|android/app/build\|DS_Store\|git'
" Force ctrlp to search in directory in which vim is opened, rather than
" travel up the tree and find .git folder, which limits its search to given
" git repository.
let g:ctrlp_working_path_mode=0

" Settings for Syntastic syntax checking
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_coffee_checkers = ['coffee']
let g:syntastic_html_tidy_ignore_errors = [
    \"trimming empty <i>",
    \"trimming empty <span>",
    \"<input> proprietary attribute \"autocomplete\"",
    \"proprietary attribute \"role\"",
    \"proprietary attribute \"hidden\"",
    \"proprietary attribute \"ng-\"",
    \"proprietary attribute \"mat-\"",
    \]"]

" Settings for Solarized scheme
set background=dark
let g:solarized_termtrans=1
let g:solarized_termcolors=256
colorscheme solarized

" Settings for indentline
let g:indentLine_enabled = 0
let g:indentLine_setColors = 0
let g:indentLine_char = '|'
let g:indentLine_color_term = 100

map <C-h> :bp<CR>
map <C-l> :bn<CR>

nnoremap <leader>q :bp<cr>:bd #<cr>

set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=2
set foldcolumn=4

" Settings for vim-indent-guides
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=235
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=237

" Settings for vim-jsx
let g:jsx_ext_required = 0
" autocmd FileType javascript.jsx setlocal commentstring={/*\ %s\ */}

vmap <leader>y :w! /tmp/vitmp<CR>                                                                   
nmap <leader>p :r! cat /tmp/vitmp<CR>

:nnoremap gR :grep '\b<cword>\b' *<CR>
:nnoremap ,f :%s/\([a-zA-Z:.]\)$/\1  /gc<CR>

" List contents of all registers (that typically contain pasteable text).
nnoremap <silent> "" :registers "0123456789abcdefghijklmnopqrstuvwxyz*+.<CR>

au VimEnter * wincmd l

runtime macros/matchit.vim

" Settings for sbdchd/neoformat, which is vim plugin for prettier-eslint
" autocmd BufWritePre *.js Neoformat
