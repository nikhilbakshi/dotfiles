#-----MAC Specific Stuff-----
PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"

MANPATH="/usr/local/opt/coreutils/libexec/gnuman:$MANPATH"
#----------------------------

# Enable programmable completion features.
if [ -f /etc/bash_completion ]; then
    source /etc/bash_completion
fi
 
# Set the PS1 prompt (with colors).
# Based on http://www-128.ibm.com/developerworks/linux/library/l-tip-prompt/
# And http://networking.ringofsaturn.com/Unix/Bash-prompts.php .
#PS1="\[\e[36;1m\]\h:\[\e[32;1m\]\w$ \[\e[0m\]"
PS1="\[\e[01;35m\]\u\[\e[0m\]\[\e[01;31m\]@\[\e[0m\]\[\e[01;35m\]\h:\[\e[32;1m\]\w \[\e[0m\]$ \[\e[0m\]"
 
# Set the default editor to vim.
export EDITOR=vim
 
# Avoid succesive duplicates in the bash command history.
export HISTCONTROL=ignoredups
 
# Append commands to the bash command history file (~/.bash_history)
# instead of overwriting it.
shopt -s histappend
 
# Append commands to the history every time a prompt is shown,
# instead of after closing the session.
PROMPT_COMMAND='history -a'
 
# Add bash aliases.
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

#eval `dircolors ~/.dir_colors`
test -e ~/.dircolors && \ 
   eval `dircolors -b ~/.dircolors`

rmd () {
  pandoc $1 | lynx -stdin
}

#alias ls="ls --color=always" 
alias ls="gls --color=always" 
alias grep="grep --color=always"
alias egrep="egrep --color=always"
alias tmux="TERM=xterm-256color /usr/local/bin/tmux"
#alias vim="/Applications/MacVim.app/Contents/bin/mvim -v"
alias vim="/usr/local/bin/mvim -v"

export ANDROID_HOME=$HOME/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

export PATH=$PATH:~/scripts
for f in ~/scripts/*.bash; do source $f; done
export JAVA_HOME=`/usr/libexec/java_home`
export PATH=$PATH:~/Software/apache-maven-3.5.4/bin
export PYTHONSTARTUP=~/.pythonrc.py
export NVM_DIR="/Users/nbakshi/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm

export WORKON_HOME=~/Envs
VIRTUALENVWRAPPER=/usr/local/bin/virtualenvwrapper.sh
if test -f "$VIRTUALENVWRAPPER"; then
    source $VIRTUALENVWRAPPER
fi
